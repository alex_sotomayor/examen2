CREATE DATABASE Tienda_Mascotas;
USE Tienda_Mascotas;

CREATE TABLE Producto(
	ProCodigo			INT PRIMARY KEY,
	ProNombre			NVARCHAR(50),
	ProCodigoBarra		INT,
	ProPrecio			INT
)

CREATE TABLE Cliente(
	CliRut				INT PRIMARY KEY,
	CliNombre			NVARCHAR(50),
	CliApellidoPaterno  NVARCHAR(50),
	CliApellidoMaterno  NVARCHAR(50),
	CliDireccion		NVARCHAR(50)
)

CREATE TABLE Mascota(
	MasCodigo			INT PRIMARY KEY,
	MasNombre			NVARCHAR(50),
	MasRaza				NVARCHAR(50),
	MasEspecie			NVARCHAR(50),
	CliRut				INT FOREIGN KEY (CliRut) REFERENCES Cliente(CliRut)
)

CREATE TABLE Venta(
	VenId				INT PRIMARY KEY IDENTITY(1,1),
	VenFolio			INT,
	CliRut				INT FOREIGN KEY (CliRut) REFERENCES Cliente(CliRut),
	MasCodigo			INT FOREIGN KEY (MasCodigo) REFERENCES Mascota(MasCodigo),
	VenFecha			DATE,
	VenNeto				INT,
	VenIva				INT,
	VenTotal			INT
)

CREATE TABLE Detalle_Venta(
	DeTid				INT PRIMARY KEY IDENTITY(1,1),
	ProCodigo			INT FOREIGN KEY (ProCodigo) REFERENCES Producto(ProCodigo),
	VenId				INT FOREIGN KEY (VenId) REFERENCES Venta(VenId),
	DetCantidad			INT,
	DetPrecioUnitario	INT,
	DetPrecioTotal		INT
)

select * from Producto

INSERT INTO Producto values
(1,	'Vacunas Anti-parasitarias',		       1234,	 6000),
(2,	'Collar para mascotas anti-pulgas',        1111,	 1450),
(3,	'Alimento de perro KANKHAN',               2222,	 3000)

select * from Cliente

INSERT INTO Cliente values
(16958412, 'Juan',     'Perez',   'Atero',    'Ines de Suarez'),
(13241454, 'Maria',     'Soto',    'Diaz',        'Las Mentas'),
(10235875, 'Daniela',  'Ya�ez',  'Angulo',      'Las Gaviotas')

select * from Mascota

INSERT INTO Mascota VALUES
(1,'Jack',      'Pastor Aleman',      'Perro',     13241454),
(2,'Tom',               'Persa',       'Gato',     16958412),
(3,'Molly',         'Britanico',       'Gato',     10235875)


select * from Venta


ALTER VIEW VW_Venta
AS
select Venta.VenFolio AS Folio,
       Venta.VenFecha AS Fecha,
	   Cliente.CliNombre +' '+ Cliente.CliApellidoPaterno AS Cliente,
	   Mascota.MasNombre +' '+ Mascota.MasEspecie AS Mascota,
	   Venta.VenTotal AS Venta
from   Venta
	   INNER JOIN Cliente ON Cliente.CliRut = Venta.CliRut
	   INNER JOIN Mascota ON Mascota.CliRut = Venta.CliRut

SELECT * FROM Detalle_Venta

ALTER VIEW VW_DetalleVenta
AS
select Venta.VenFolio AS Folio,
       Venta.VenFecha AS Fecha,
	   Cliente.CliNombre +' '+ Cliente.CliApellidoPaterno AS Cliente,
	   Detalle_Venta.DetPrecioTotal  AS Venta
from   Venta
	   INNER JOIN Cliente ON Cliente.CliRut = Venta.CliRut
	   INNER JOIN Detalle_Venta ON Detalle_Venta.VenId = Venta.VenId

select * from VW_DetalleVenta


ALTER VIEW VW_InfoVenta
AS
SELECT Detalle_Venta.DetCantidad        AS Cantidad,
       Producto.ProNombre               AS Producto,
	   Producto.ProPrecio               AS [$ Unitario],
	   Detalle_Venta.DetPrecioTotal     AS Precio
FROM   Detalle_Venta
       INNER JOIN Producto ON Producto.ProCodigo = Detalle_Venta.ProCodigo

SELECT * FROM VW_InfoVenta
