/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.aiep.servlet;

import cl.aiep.conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alex
 */
public class GuardarDetalleVenta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DetalleVenta</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DetalleVenta at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idVenta  = request.getParameter("idVenta");
        String cantidad = request.getParameter("cantidad");
        String producto = request.getParameter("producto");
        
        Conexion cnx = new Conexion();
        try {
            cnx.getConnection();
            String queryP = "SELECT ProPrecio FROM Producto Where ProCodigo='"+producto+"'";
            ResultSet productoPrecio = cnx.EntregaDatos(queryP);
            int precio = 0;
            
            while(productoPrecio.next()){
                precio = productoPrecio.getInt(1);
            }
            int precioTotal = precio - (precio * (Integer.parseInt(cantidad)));
            
                    String queryGuardar = "insert into Detalle_Venta "+
                       "(DetCantidad, ProCodigo, DetPrecioUnitario, DetPrecioTotal, DeTid) "+
                       "values "+
                       "('"+cantidad+"', '"+producto+"', '"+precio+"', '"+precioTotal+"', '"+idVenta+"')";
            cnx.guardarDatos(queryGuardar);
            response.sendRedirect("DetalleVenta.jsp?VenId="+idVenta);
            
        } catch (SQLException ex) {
            Logger.getLogger(GuardarDetalleVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
