
package cl.aiep.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Conexion {
    private String server;
    private String user;
    private String base;
    private String clave;
    private int port;
    private String url;

    private Connection conexion;

    public Conexion() {
        this.clave = "123";
        this.server = "localhost";
        this.user = "alex";
        this.port = 1433;
        this.base = "Tienda_Mascotas";

        this.url = "jdbc:sqlserver://localhost:1433;databaseName=Tienda_Mascotas;";

    }

    public Connection getConnection() throws SQLException {
        this.conexion = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            this.conexion = DriverManager.getConnection(this.url, this.user, this.clave);
            System.out.println(" exito al conectarse");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error de conexion : " + ex.getMessage());
        }
        return this.conexion;
    }

    public ResultSet EntregaDatos(String query) throws SQLException {
        return this.conexion.createStatement().executeQuery(query);
    }

    public ResultSet cargarCombo(String query) throws SQLException {
        return this.conexion.createStatement().executeQuery(query);
    }

    public boolean guardarDatos(String query) throws SQLException {
        return this.conexion.createStatement().execute(query);
    }

    public boolean devolverDatos(String query) throws SQLException {
        return this.conexion.createStatement().execute(query);
    }
}
