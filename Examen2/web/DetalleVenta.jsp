<%-- 
    Document   : DetalleVenta
    Created on : 14-sep-2019, 3:35:31
    Author     : Alex
--%>

<%@page import="cl.aiep.conexion.Conexion"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Conexion cnx = new Conexion();
    cnx.getConnection();
    String idVenta = request.getParameter("VenId");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Detalle de Venta</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <h1>Información de Venta</h1>

        <table class="table table-hover col-md-6">
            <tr>
                <th>Folio</th>
                <th>Fecha </th>
                <th>Cliente </th>
                <th>Total</th>
            </tr>
            <%
                ResultSet detalle = cnx.cargarCombo("SELECT * FROM VW_DetalleVenta");

                while (detalle.next()) {

                    out.println("<tr>");
                    out.println("<td>" + detalle.getInt(1) + "</td>");
                    out.println("<td>" + detalle.getString(2) + "</td>");
                    out.println("<td>" + detalle.getString(3) + "</td>");
                    out.println("<td>" + detalle.getString(4) + "</td>");
                    out.println("</tr>");
                }
            %>
        </table>

        <h1>Información de Venta</h1>
        <form action="GuardarDetalleVenta" method="post">
            <div class="form mt-5">
                <div class="form-row">
                    <input type="hidden" value="<%=idVenta%>" name="IdVenta"/>
                    <div class="form-group col-md-6">
                        <label for="cantidad">Cantidad </label>
                        <input type="text" name="cantidad" id="cantidad" class="form-control" placeholder="Cantidad" />
                    </div>

                    <div class="form-group col-md-6">

                        <label for="producto"> Producto</label>
                        <select name="producto" id="producto" class="form-control">
                            <option value="0">Seleccione un Producto</option>
                            <%                       ;
                                ResultSet puntero = cnx.EntregaDatos("SELECT  ProCodigo, ProNombre FROM producto");

                                while (puntero.next()) {
                                    out.println("<option value='" + puntero.getInt(1) + "'>" + puntero.getString(2) + "</option>");
                                }

                            %> 
                        </select>
                    </div> 
                </div>
            </div>
             <form action="EliminarDetalle" id="frmEliminar" method="POST">
                <input type="hidden" id="IdDetalleVenta"  name="IdDetalleVenta"/>
                <input type="hidden" value="<%=idVenta%>" name="IdVenta"/>
             </form> 
            <table class="table table-hover col-md-6">
                <tr>
                    <th>Cantidad</th>
                    <th>Poducto </th>
                    <th>$ Unitario </th>
                    <th>$ Total</th>
                    <th>Eliminar</th>
                </tr>
                <%
                    String querytab = "select "+
                             "Detalle_Venta.DetCantidad  AS Cantidad, "+
                             "Producto.ProNombre               AS Producto, "+
                             "Producto.ProPrecio               AS [$ Unitario], "+
                             "Detalle_Venta.DetPrecioTotal     AS Precio "+
                             "FROM   Detalle_Venta "+
                             "INNER JOIN Producto ON Producto.ProCodigo = Detalle_Venta.ProCodigo "+
                             "where Detalle_Venta.DeTid =" + idVenta;

                    ResultSet infoVenta = cnx.EntregaDatos(querytab);

                    while (infoVenta.next()) {

                        out.println("<tr>");
                        out.println("<td>" + infoVenta.getInt(1) + "</td>");
                        out.println("<td>" + infoVenta.getString(2) + "</td>");
                        out.println("<td>" + infoVenta.getInt(3) + "</td>");
                        out.println("<td>" + (infoVenta.getInt(1) * infoVenta.getInt(3)) + "</td>");
                        out.println("<td><input type='button' class='btn btn-primary' value='quitar' onClick='QuitaProducto(" + infoVenta.getInt(4) + ")'/></td>");
                        out.println("</tr>");
                    }
                %>
            </table>        

            <form action="IngresoVenta.jsp">
                     <input type="submit" value="Finalizar" class="btn btn-success">
                </form> 
            
        </form>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
