
<%@page import="java.sql.ResultSet"%>
<%@page import="cl.aiep.conexion.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Conexion cnx = new Conexion();
    cnx.getConnection();

    String Ventaid = request.getParameter("VenId");
    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso de Venta</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="container mt-5">
            <h1>Ventas</h1>
            <form action="IngresoVenta" method="post">
                <input type="hidden" value="<%=Ventaid%>" name="IdVenta"/>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="folio">Folio</label>
                        <input type="text" class="form-control" id="folio" name="folio" >
                    </div>

                    <div class="form-group col-md-6">
                        <label for="fecha">Fecha </label>
                        <input type="date" name="fecha" id="fecha" class="form-control"  />
                    </div> 

                    <div class="form-group col-md-6">
                        <label for="cliente"> Cliente</label>
                        <select name="cliente" id="cliente" class="form-control">
                            <option value="0">Seleccione un Cliente</option>
                            <%                        ResultSet cliente = cnx.cargarCombo("select CliRut, CliNombre +' '+ CliApellidoPaterno from Cliente ");

                                while (cliente.next()) {
                                    out.println("<option value='" + cliente.getInt(1) + "'>");
                                    out.println(cliente.getString(2));
                                    out.println("</option>");
                                }
                            %>
                        </select>
                    </div> 

                    <div class="form-group col-md-6">
                        <label for="mascota"> Mascota</label>
                        <select name="mascota" id="mascota" class="form-control">
                            <option value="0">Seleccione una mascota</option>
                            <%
                                ResultSet mascota = cnx.cargarCombo("select MasCodigo, Mascota.MasNombre+' -- '+ Mascota.MasEspecie from Mascota");

                                while (mascota.next()) {
                                    out.println("<option value='" + mascota.getInt(1) + "'>");
                                    out.println(mascota.getString(2));
                                    out.println("</option>");
                                }
                            %>
                        </select>
                    </div>

                    <div class="form-group col-md-4"> 
                        <input type="reset" value="Limpiar" class="btn btn-success" />
                    </div>    

                    <div class="form-group col-md-8"> 
                        <input type="submit" value="Guardar" class="btn btn-success" />
                    </div> 
                </div>
            </form>
                        
 </form>
                            <form action="EliminarDetalle" id="frmEliminar" method="POST">
                                <input type="hidden" id="IdDetalleVenta"  name="IdDetalleVenta"/>
                                <input type="hidden" value="<%=Ventaid%>" name="IdVenta"/>
                            </form> 
                      
            <table class="table table-hover">
                <tr>
                    <th>Folio</th>
                    <th>Fecha </th>
                    <th>Cliente </th>
                    <th>Mascota </th>
                    <th>Venta </th> 
                    <th>Ver </th>
                </tr>
                <%
                ResultSet venta = cnx.cargarCombo("SELECT * FROM VW_Venta");   
                   
                    while (venta.next()) {

                        out.println("<tr>");
                        out.println("<td>" + venta.getInt(1) + "</td>");
                        out.println("<td>" + venta.getString(2) + "</td>");
                        out.println("<td>" + venta.getString(3) + "</td>");
                        out.println("<td>" + venta.getString(4) + "</td>");
                        out.println("<td>" + venta.getInt(5) + "</td>");
                        out.println("<td><input type='button' class='btn btn-primary' value='Ver' onClick='Detalleventa.jsp'/></td>");
                        out.println("</tr>");
                    }

                %>
            </table>
            
            </form>  
                <%
                    if( request.getParameter("Error") !=null){
                        %>
                        <div class ="alert alert-danger" roler="alert">
                            ¡Folio   Ya existe en el sistema, por favor ingresar otro folio!!!
                        </div>
                        <%
                    }
                %>
        </div>
       
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
