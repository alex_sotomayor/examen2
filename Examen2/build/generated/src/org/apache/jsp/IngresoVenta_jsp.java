package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import cl.aiep.conexion.Conexion;

public final class IngresoVenta_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    Conexion cnx = new Conexion();
    cnx.getConnection();

    String Ventaid = request.getParameter("VenId");
    

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Ingreso de Venta</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container mt-5\">\n");
      out.write("            <h1>Ventas</h1>\n");
      out.write("            <form action=\"IngresoVenta\" method=\"post\">\n");
      out.write("                <input type=\"hidden\" value=\"");
      out.print(Ventaid);
      out.write("\" name=\"IdVenta\"/>\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"folio\">Folio</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" id=\"folio\" name=\"folio\" >\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"fecha\">Fecha </label>\n");
      out.write("                        <input type=\"date\" name=\"fecha\" id=\"fecha\" class=\"form-control\"  />\n");
      out.write("                    </div> \n");
      out.write("\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"cliente\"> Cliente</label>\n");
      out.write("                        <select name=\"cliente\" id=\"cliente\" class=\"form-control\">\n");
      out.write("                            <option value=\"0\">Seleccione un Cliente</option>\n");
      out.write("                            ");
                        ResultSet cliente = cnx.cargarCombo("select CliRut, CliNombre +' '+ CliApellidoPaterno from Cliente ");

                                while (cliente.next()) {
                                    out.println("<option value='" + cliente.getInt(1) + "'>");
                                    out.println(cliente.getString(2));
                                    out.println("</option>");
                                }
                            
      out.write("\n");
      out.write("                        </select>\n");
      out.write("                    </div> \n");
      out.write("\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"mascota\"> Mascota</label>\n");
      out.write("                        <select name=\"mascota\" id=\"mascota\" class=\"form-control\">\n");
      out.write("                            <option value=\"0\">Seleccione una mascota</option>\n");
      out.write("                            ");

                                ResultSet mascota = cnx.cargarCombo("select MasCodigo, Mascota.MasNombre+' -- '+ Mascota.MasEspecie from Mascota");

                                while (mascota.next()) {
                                    out.println("<option value='" + mascota.getInt(1) + "'>");
                                    out.println(mascota.getString(2));
                                    out.println("</option>");
                                }
                            
      out.write("\n");
      out.write("                        </select>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"form-group col-md-4\"> \n");
      out.write("                        <input type=\"reset\" value=\"Limpiar\" class=\"btn btn-success\" />\n");
      out.write("                    </div>    \n");
      out.write("\n");
      out.write("                    <div class=\"form-group col-md-8\"> \n");
      out.write("                        <input type=\"submit\" value=\"Guardar\" class=\"btn btn-success\" />\n");
      out.write("                    </div> \n");
      out.write("                </div>\n");
      out.write("            </form>\n");
      out.write("                        \n");
      out.write(" </form>\n");
      out.write("                            <form action=\"EliminarDetalle\" id=\"frmEliminar\" method=\"POST\">\n");
      out.write("                                <input type=\"hidden\" id=\"IdDetalleVenta\"  name=\"IdDetalleVenta\"/>\n");
      out.write("                                <input type=\"hidden\" value=\"");
      out.print(Ventaid);
      out.write("\" name=\"IdVenta\"/>\n");
      out.write("                            </form> \n");
      out.write("                      \n");
      out.write("            <table class=\"table table-hover\">\n");
      out.write("                <tr>\n");
      out.write("                    <th>Folio</th>\n");
      out.write("                    <th>Fecha </th>\n");
      out.write("                    <th>Cliente </th>\n");
      out.write("                    <th>Mascota </th>\n");
      out.write("                    <th>Venta </th> \n");
      out.write("                    <th>Ver </th>\n");
      out.write("                </tr>\n");
      out.write("                ");

                ResultSet venta = cnx.cargarCombo("SELECT * FROM VW_Venta");   
                   
                    while (venta.next()) {

                        out.println("<tr>");
                        out.println("<td>" + venta.getInt(1) + "</td>");
                        out.println("<td>" + venta.getString(2) + "</td>");
                        out.println("<td>" + venta.getString(3) + "</td>");
                        out.println("<td>" + venta.getString(4) + "</td>");
                        out.println("<td>" + venta.getInt(5) + "</td>");
                        out.println("<td><input type='button' class='btn btn-primary' value='Ver' onClick='Detalleventa.jsp'/></td>");
                        out.println("</tr>");
                    }

                
      out.write("\n");
      out.write("            </table>\n");
      out.write("            \n");
      out.write("            </form>  \n");
      out.write("                ");

                    if( request.getParameter("Error") !=null){
                        
      out.write("\n");
      out.write("                        <div class =\"alert alert-danger\" roler=\"alert\">\n");
      out.write("                            ¡Folio   Ya existe en el sistema, por favor ingresar otro folio!!!\n");
      out.write("                        </div>\n");
      out.write("                        ");

                    }
                
      out.write("\n");
      out.write("        </div>\n");
      out.write("       \n");
      out.write("        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
