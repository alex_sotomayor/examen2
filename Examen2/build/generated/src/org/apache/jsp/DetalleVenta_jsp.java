package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import cl.aiep.conexion.Conexion;
import java.sql.ResultSet;

public final class DetalleVenta_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    Conexion cnx = new Conexion();
    cnx.getConnection();
    String idVenta = request.getParameter("VenId");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Detalle de Venta</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <h1>Información de Venta</h1>\n");
      out.write("\n");
      out.write("        <table class=\"table table-hover col-md-6\">\n");
      out.write("            <tr>\n");
      out.write("                <th>Folio</th>\n");
      out.write("                <th>Fecha </th>\n");
      out.write("                <th>Cliente </th>\n");
      out.write("                <th>Total</th>\n");
      out.write("            </tr>\n");
      out.write("            ");

                ResultSet detalle = cnx.cargarCombo("SELECT * FROM VW_DetalleVenta");

                while (detalle.next()) {

                    out.println("<tr>");
                    out.println("<td>" + detalle.getInt(1) + "</td>");
                    out.println("<td>" + detalle.getString(2) + "</td>");
                    out.println("<td>" + detalle.getString(3) + "</td>");
                    out.println("<td>" + detalle.getString(4) + "</td>");
                    out.println("</tr>");
                }
            
      out.write("\n");
      out.write("        </table>\n");
      out.write("\n");
      out.write("        <h1>Información de Venta</h1>\n");
      out.write("        <form action=\"GuardarDetalleVenta\" method=\"post\">\n");
      out.write("            <div class=\"form mt-5\">\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <input type=\"hidden\" value=\"");
      out.print(idVenta);
      out.write("\" name=\"IdVenta\"/>\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("                        <label for=\"cantidad\">Cantidad </label>\n");
      out.write("                        <input type=\"text\" name=\"cantidad\" id=\"cantidad\" class=\"form-control\" placeholder=\"Cantidad\" />\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"form-group col-md-6\">\n");
      out.write("\n");
      out.write("                        <label for=\"producto\"> Producto</label>\n");
      out.write("                        <select name=\"producto\" id=\"producto\" class=\"form-control\">\n");
      out.write("                            <option value=\"0\">Seleccione un Producto</option>\n");
      out.write("                            ");
                       ;
                                ResultSet puntero = cnx.EntregaDatos("SELECT  ProCodigo, ProNombre FROM producto");

                                while (puntero.next()) {
                                    out.println("<option value='" + puntero.getInt(1) + "'>" + puntero.getString(2) + "</option>");
                                }

                            
      out.write(" \n");
      out.write("                        </select>\n");
      out.write("                    </div> \n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("             <form action=\"EliminarDetalle\" id=\"frmEliminar\" method=\"POST\">\n");
      out.write("                <input type=\"hidden\" id=\"IdDetalleVenta\"  name=\"IdDetalleVenta\"/>\n");
      out.write("                <input type=\"hidden\" value=\"");
      out.print(idVenta);
      out.write("\" name=\"IdVenta\"/>\n");
      out.write("             </form> \n");
      out.write("            <table class=\"table table-hover col-md-6\">\n");
      out.write("                <tr>\n");
      out.write("                    <th>Cantidad</th>\n");
      out.write("                    <th>Poducto </th>\n");
      out.write("                    <th>$ Unitario </th>\n");
      out.write("                    <th>$ Total</th>\n");
      out.write("                    <th>Eliminar</th>\n");
      out.write("                </tr>\n");
      out.write("                ");

                    String querytab = "select "+
                             "Detalle_Venta.DetCantidad  AS Cantidad, "+
                             "Producto.ProNombre               AS Producto, "+
                             "Producto.ProPrecio               AS [$ Unitario], "+
                             "Detalle_Venta.DetPrecioTotal     AS Precio "+
                             "FROM   Detalle_Venta "+
                             "INNER JOIN Producto ON Producto.ProCodigo = Detalle_Venta.ProCodigo "+
                             "where Detalle_Venta.DeTid =" + idVenta;

                    ResultSet infoVenta = cnx.EntregaDatos(querytab);

                    while (infoVenta.next()) {

                        out.println("<tr>");
                        out.println("<td>" + infoVenta.getInt(1) + "</td>");
                        out.println("<td>" + infoVenta.getString(2) + "</td>");
                        out.println("<td>" + infoVenta.getInt(3) + "</td>");
                        out.println("<td>" + (infoVenta.getInt(1) * infoVenta.getInt(3)) + "</td>");
                        out.println("<td><input type='button' class='btn btn-primary' value='quitar' onClick='QuitaProducto(" + infoVenta.getInt(4) + ")'/></td>");
                        out.println("</tr>");
                    }
                
      out.write("\n");
      out.write("            </table>        \n");
      out.write("\n");
      out.write("            <form action=\"IngresoVenta.jsp\">\n");
      out.write("                     <input type=\"submit\" value=\"Finalizar\" class=\"btn btn-success\">\n");
      out.write("                </form> \n");
      out.write("            \n");
      out.write("        </form>\n");
      out.write("        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
